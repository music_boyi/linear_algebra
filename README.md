# 线性代数学习包

#  

# 声明

本包目前限于线性代数学习时使用, 采用的计算方法和存储方法都是最原始的存储方式暂未完全优化, 如果有想法在某些功能上进行优化, 烦请拨冗惠赠相关代码到issue, 万分感谢

本包目前采用仓颉编程语言写就, 目前2024-7-14并未完全开放, 使用前请先申请sdk.

# 使用方法

## 建立仓颉项目(cjpm)添加依赖

在空文件夹中使用 cjpm 建立仓颉项目.

```bash
cjpm init
```

之后会在当前目录下生成一系列文件和文件夹

```bash
.
├── cjpm.toml
└── src
    └── main.cj
```

尝试运行`cjpm run` 如果有输出内容则代表项目创建成功.

打开 `cjpm.toml` 文件

在 `dependencies` 项目中, 输入下列内容

```toml
linear = { git = "https://gitcode.com/music_boyi/linear_algebra.git", tag = "va.b"}
```

请查看项目tag确认`a.b`的值. 或者将`, tag = "va.b"` 部分舍去

![image-20240715164640683](https://raw.githubusercontent.com/nanshowpers/tuch/imag/imagimage-20240715164640683.png)

## 使用方法

### package: mat

#### class : Matrix

矩阵类, 包含了矩阵的初始化, 访问, 和最基本运算功能.

##### 初始化

初始化采用三种方式: 无参构造和有参构造. 有参构造参数是命名参数, 必须提供参数名称.

```cangjie
var mat : Matrix = Matrix() // 初始化, 0行, 0列, 无数据
var mat : Matrix = Matrix(cols:2, rows: 3) // 3行, 2列, 数据全0
var mat : Matrix = Matrix([[1, 2, 3]]) // 1行, 3列, 数据是 [[1, 2, 3]]
```

**注意第三种方式, 数据内容必须是一个二维数组, 必须有两层 [ ]**

## Package: mat.calc

#### interface:  MatrixCalcI

这个接口负责扩展矩阵的基本运算功能, 包括加减乘除四则运算功能.

##### 计算

支持矩阵之间的加减乘除运算, 矩阵加减乘支持矩阵和矩阵, 矩阵和数运算. 矩阵除法运算只支持矩阵和数相除. 矩阵乘法按照线性代数中矩阵乘法进行.

#### interface: MatrixCalcII

这个接口负责扩展矩阵的一些特性的计算, 比如行列式, 比如矩阵的秩, 比如特征值和特征向量, 比如奇异值分解, 以及矩阵的逆等.

## 示例(假设建立名为"yourprjname"的module)

```cangjie
package yourprjname
import linear.mat.Matrix
import std.collection.ArrayList
import linear.mat.calc.MatrixCalcI

main() {
    var c : Matrix
    var b : Matrix
    
    c = Matrix([
        [1, 2], 
        [3, 4], 
        [5, 6]
    ])

    b = Matrix([
        [7, 8, 9],
        [10, 11, 12]
    ])
    
    
    var d = c*b

    d.Show()

    


}

```







# 更新纪录和目前存在的问题

## 2024-07-16

+   仓颉语言更新, 适配了新的版本语言
+   更新了readme 中演示代码

## 2024-07-15

+   实现了矩阵乘法功能,
+   支持参数中输入整数, 程序内部会转换为浮点数.
+   删除了 setData 操作



## 2024-07-14

目前只有矩阵相关功能, 并且尚不完全, 矩阵乘法功能仍未实现
